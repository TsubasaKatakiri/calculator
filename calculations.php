<?php

//new code starts here
$actions = $_POST['actions'];

if (count($actions) >= 3) {
    $calc = array_slice($actions, 0, count($actions));
    //check and rebuild parts of array with special operators
    for ($i = 0; $i < count($calc); $i++) {
        if ($calc[$i] == 'pow') {
            $j = $i - 1;
            $k = $i + 1;
            $temp = ['pow', '(', "$calc[$j]", ',', "$calc[$k]", ')'];
            array_splice($calc, $j, 3, $temp);
        } elseif ($calc[$i] == 'sqrt') {
            $j = $i - 1;
            $temp = ['sqrt', '(', "$calc[$j]", ')'];
            array_splice($calc, $j, 2, $temp);
        }
    }
    $string = implode("", $calc);
    //check if result string ends with sign
    if(preg_match('/[\+\-\*\/]/', substr($string, -1))==true){
        $string=substr($string, 0, -1);
    }
    //checking string for lost brackets and deleting them
    $lbracket=false;
    if(strpos($string, "(")!==false){
        $lbracket=true;
    }
    $rbracket=false;
    if(strpos($string, ")")!==false){
        $rbracket=true;
    }
    if($lbracket==false && $lbracket!=$rbracket){
        $string=str_replace(")", "", $string);
    }
    $exec = 'print (' . $string . ');';
    $result = eval($exec);
    echo $result;
}