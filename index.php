<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="style.css">
    <title>Calculator</title>
</head>
<body>
<div class="calculator-container">
    <div class="container">
        <input type="text" id="out" placeholder="0" readonly>
        <div class="row">
            <div class="col-sm-6">
                <button class="erase wide" id="clear">C</button>
            </div>
            <div class="col-sm-3">
                <button class="erase" id="back"><</button>
            </div>
            <div class="col-sm-3">
                <button class="actions" id="sqrt">sqrt</button>
            </div>
        </div>
        <div class="row">
            <div class="col-sm">
                <button class="actions" id="lbracket">(</button>
            </div>
            <div class="col-sm">
                <button class="actions" id="rbracket">)</button>
            </div>
            <div class="col-sm">
                <button class="actions" id="pow">pow</button>
            </div>
            <div class="col-sm">
                <button class="actions" id="plus">+</button>
            </div>
        </div>
        <div class="row">
            <div class="col-sm">
                <button class="numbers" id="but7">7</button>
            </div>
            <div class="col-sm">
                <button class="numbers" id="but8">8</button>
            </div>
            <div class="col-sm">
                <button class="numbers" id="but9">9</button>
            </div>
            <div class="col-sm">
                <button class="actions" id="minus">-</button>
            </div>
        </div>
        <div class="row">
            <div class="col-sm">
                <button class="numbers" id="but4">4</button>
            </div>
            <div class="col-sm">
                <button class="numbers" id="but5">5</button>
            </div>
            <div class="col-sm">
                <button class="numbers" id="but6">6</button>
            </div>
            <div class="col-sm">
                <button class="actions" id="multiply">*</button>
            </div>
        </div>
        <div class="row">
            <div class="col-sm">
                <button class="numbers" id="but1">1</button>
            </div>
            <div class="col-sm">
                <button class="numbers" id="but2">2</button>
            </div>
            <div class="col-sm">
                <button class="numbers" id="but3">3</button>
            </div>
            <div class="col-sm">
                <button class="actions" id="divide">/</button>
            </div>
        </div>
        <div class="row">
            <div class="col-sm">
                <button class="numbers" id="negative">+/-</button>
            </div>
            <div class="col-sm">
                <button class="numbers" id="but0">0</button>
            </div>
            <div class="col-sm">
                <button class="numbers" id="point">.</button>
            </div>
            <div class="col-sm">
                <button class="finalize" id="calc">=</button>
            </div>
        </div>
    </div>
</div>
<script>
    //variables section
    let actions = []; //array that will store all numbers and variables;
    let element = ""; //array element

    //send function
    function send(end) {
        if (actions.length >= 3 && (!actions.includes("(") || (actions.includes("(") && actions.includes(")")))) {
            $.ajax({
                type: 'POST',
                url: 'calculations.php',
                data: {actions: actions},
                success: function (result) {
                    console.log(actions);
                    actions.splice(0, (actions.length - 1), result);
                    if (end == true) actions = [];
                    $("#out").val(result);
                }
            });
        }
    }

    //clear function
    function clear() {
        actions = [];
        element = "";
    }

    //correcting multiple signs in the end of array / replacing signs
    function signChecker() {
        let act = ["+", '-', "*", "/", "pow"];
        let isSignPreLast = act.includes(actions[actions.length - 2]);
        if (isSignPreLast == true && actions[actions.length - 1] != "(") {
            let temp = actions[actions.length - 1];
            actions.splice(-2, 2, temp);
        }
    }

    //decoding meanings of buttons
    function buttonDecoder(button) {
        let thisID = $(button).attr('id');
        let text = $(`#${thisID}`).text();
        return text;
    }

    //number buttons handler
    $(".numbers").on("click", function () {
        let text = buttonDecoder(this);
        if (text == "+/-") {
            if (element[0] == "-") element = element.substring(1);
            else element = "-" + element;
        } else {
            element += text;
        }
        $('#out').val(element);
    });

    //action buttons handler
    $(".actions").on("click", function () {
        let sign = buttonDecoder(this);
        if (element != "") actions.push(element);
        actions.push(sign);
        if (sign == "sqrt") {
            actions.push('');
        }
        signChecker();
        if (sign != "pow") send(false);
        element = "";
    });

    //calc (=) button handler
    $('#calc').on('click', function () {
        actions.push(element);
        send(true);
        clear();
    });

    //back and clear button handlers
    $('#back').on('click', function () {
        element = element.substring(0, element.length - 1);
        $('#out').val(element);
    });

    $('#clear').on('click', function () {
        clear();
        $('#out').val(element);
    });

</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
        crossorigin="anonymous"></script>
</body>
</html>